package com.example.ecleticsnews


import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cn.pedant.SweetAlert.SweetAlertDialog
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_news.*
import org.json.JSONException


class NewsActivity : AppCompatActivity() {
    private val mRecyclerView: RecyclerView? = null
    private var mExampleAdapter: NewsAdapter? = null
    private var mExampleList: ArrayList<NewsModel>? = null
    private var mRequestQueue: RequestQueue? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)
        //setting my list items for my recycler view
//        val listRecycler = generateDummyList()
        //ref the recycler view widget and set the adapter to it
//        coronaRecycler.adapter = CoronaAdapter(this,listRecycler)
        //give our recycler items a view group
        newsRecycler.layoutManager = LinearLayoutManager(this)
        //set a fixed item size
        newsRecycler.setHasFixedSize(true)

        mExampleList = ArrayList()
        mRequestQueue = Volley.newRequestQueue(this)
        fetchData()

    }

    private fun fetchData(){
        val loadingDialog = SweetAlertDialog(this@NewsActivity, SweetAlertDialog.PROGRESS_TYPE)
        loadingDialog.setTitleText("Loading News Articles ...") //Processing your request
        loadingDialog.setCancelable(true)
        loadingDialog.setCanceledOnTouchOutside(false)
        loadingDialog.show()
        val url = "https://newsapi.org/v2/everything?q=bitcoin&apiKey=3a3d6394fa20414f928fc423bc335df4"

        val request = object: JsonObjectRequest(
            Request.Method.GET, url, null,
            Response.Listener { response ->
                loadingDialog.dismiss()
                try {
                    val jsonArray = response.getJSONArray("articles")
                    Log.d("array", "response is $jsonArray")
                    for (i in 0 until jsonArray.length()) {
                        val hit = jsonArray.getJSONObject(i)
                        val jsonObjectSource = hit.getJSONObject("source")
                        val newsName = jsonObjectSource.getString("name")
                        val newsAuthor = hit.getString("author")
                        val newsTitle = hit.getString("title")
                        val newsLink = hit.getString("url")
                        val newsDate = hit.getString("publishedAt")
                        val newsImage = hit.getString("urlToImage")
                        val newsDescription = hit.getString("description")
                        val newsContent = hit.getString("content")

                        mExampleList!!.add(
                            NewsModel(
                                newsName,
                                newsAuthor,
                                newsTitle,
                                newsLink,
                                newsDescription,
                                newsImage,
                                newsDate,
                                newsContent
                            )
                        )
                    }
                    //adding model data to adapter
                    mExampleAdapter = mExampleList?.let { NewsAdapter(this@NewsActivity, it) }
                    newsRecycler!!.adapter = mExampleAdapter
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }, Response.ErrorListener {
                    error -> error.printStackTrace()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["User-Agent"] = "Mozilla/5.0"
                return headers
            }
        }


        mRequestQueue!!.add(request)
    }


    @SuppressLint("MissingPermission")
    private fun isNetworkConnected(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            connectivityManager.activeNetwork
        } else {
            TODO("VERSION.SDK_INT < M")
        }
        val networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork)
        return networkCapabilities != null && networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
    }

}