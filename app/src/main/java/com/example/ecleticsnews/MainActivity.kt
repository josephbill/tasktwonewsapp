package com.example.ecleticsnews

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class MainActivity : AppCompatActivity() {
    val SPLASH_DISPLAY_LENGTH: Int = 3000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //handler class

        //handler class
        Handler().postDelayed(Runnable { //intent
            val intent = Intent(this@MainActivity, NewsActivity::class.java)
            startActivity(intent)
        }, SPLASH_DISPLAY_LENGTH.toLong())
    }


}