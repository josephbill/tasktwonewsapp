package com.example.ecleticsnews

data class NewsModel(val newsName: String, val newsAuthor: String, val newsTitle: String, val newsLink: String, val newsDesc: String,
val newsImage: String, val newsPublished: String, val newsContent: String) {
}