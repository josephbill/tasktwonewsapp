package com.example.ecleticsnews

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.news_item.view.*

class NewsAdapter(private var context: Context, private var itemList: List<NewsModel>)  : RecyclerView.Adapter<NewsAdapter.RecyclerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val inflater = LayoutInflater.from(parent.context).inflate(R.layout.news_item,parent,false)
        return RecyclerViewHolder(inflater)

    }

    override fun getItemCount() = itemList.size

    override fun onBindViewHolder(holder: NewsAdapter.RecyclerViewHolder, position: Int) {
        val items  = itemList[position]

        //setting data
        holder.newsName.text = items.newsName
        holder.newsTitle.text = items.newsTitle
        holder.newsDate.text = items.newsPublished
        holder.newsName.text = items.newsName
//        //glide to display my image
//        Glide.with(context).load(items.newsImage).into(holder.newsImage);
        Glide.with(context).load(items.newsImage).into(holder.newsImages)

        //see details
        holder.seeDetails.setOnClickListener {
            val intent  = Intent(context,NewsDetails::class.java)
            intent.putExtra("newsContent",items.newsContent)
            intent.putExtra("newsTitle",items.newsTitle)
            intent.putExtra("newsImage",items.newsImage)
            intent.putExtra("newsAuthor",items.newsAuthor)
            context.startActivity(intent)
        }

    }




    //creating view holder class to enable me to ref the views inside the recycled item
    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val newsName: TextView = itemView.newsName
        val newsTitle: TextView = itemView.newsTitle
        val newsDate: TextView = itemView.newsDate
        val seeDetails: Button = itemView.seeDetails
        val newsImages: ImageView = itemView.media_image

    }



}