package com.example.ecleticsnews

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_news_details.*

class NewsDetails : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_details)

        //getting data shared

        //picking text
        val sharedTitle: String? = intent.getStringExtra("newsTitle")
        val sharedContent: String? = intent.getStringExtra("newsContent")
        val sharedImage: String? = intent.getStringExtra("newsImage")
        val sharedAuthor: String? = intent.getStringExtra("newsAuthor")

        //setting text to view
        textDesc.text = sharedTitle
        textContent.text = sharedContent
        textAuthor.text = sharedAuthor
        Glide.with(this).load(sharedImage).into(imageNews)
    }
}